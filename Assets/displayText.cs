using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using System.IO;
public class displayText : MonoBehaviour
{
    public string api_key = "";

    // Use this for initialization
    void Start()
    {
        //CallApi();
        setupTextField();
    }

    public void setupTextField()
    {
        InputField InputField = GameObject.Find("Canvas/InputField").GetComponent<InputField>();
        var FileString = ReadFile();
        InputField.text = FileString.Replace("<br>", System.Environment.NewLine);
    }

    // Update is called once per frame
    public void Update()
    {
        Text InputText = GameObject.Find("Canvas/InputField/InputText").GetComponent<Text>();
        InputField InputField = GameObject.Find("Canvas/InputField").GetComponent<InputField>();
        TextMesh ARText = GameObject.Find("Target/Text").GetComponent<TextMesh>();

        if (ARText != null)
        {
            ARText.text = InputField.text;
        }

        if (Input.GetKeyDown("a"))
        {
            WriteFile(InputField.text);
        }
    }

    public string ReadFile()
    {
        string path = "Assets/displayText.cs";

        StreamReader reader = new StreamReader(path);
        return reader.ReadToEnd();
    }
    public void WriteFile(string NewFile)
    {
        string path = "Assets/displayText2.cs";

        File.WriteAllText(path, NewFile);
    }

    /* Create a Root Object to store the returned json data in */
    [System.Serializable]
    public class Quotes
    {
        public Quote[] values;
    }

    [System.Serializable]
    public class Quote
    {
        public string package_name;
        public string sum_assured;
        public int base_premium;
        public string suggested_premium;
        public string created_at;
        public string quote_package_id;
        public QuoteModule module;
    }

    [System.Serializable]
    public class QuoteModule
    {
        public string type;
        public string make;
        public string model;
    }

    [Serializable]
    public struct Param
    {
        public string key;
        public string value;
    }

    public void CallApi()
    {
        StartCoroutine(CreateQuoteCoroutine("iPhone 6S 64GB LTE"));
    }

    IEnumerator CreateQuoteCoroutine(String modelNr)
    {
        TextMesh textObject = GameObject.Find("Target/Text").GetComponent<TextMesh>();
        string auth = api_key + ":";
        auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
        auth = "Basic " + auth;
        WWWForm form = new WWWForm();
        form.AddField("type", "root_gadgets");
        form.AddField("model_name", modelNr);
        UnityWebRequest www = UnityWebRequest.Post("https://sandbox.root.co.za/v1/insurance/quotes", form);
        www.SetRequestHeader("AUTHORIZATION", auth);
        yield return www.Send();
        if (www.isNetworkError || www.isHttpError)
        {
            textObject.text = www.downloadHandler.text;
        }
        else
        {
            Quotes json = JsonUtility.FromJson<Quotes>("{\"values\":" + www.downloadHandler.text + "}");
            textObject.text = "Make: " + json.values[0].module.make + "\nPremium: R" + (json.values[0].base_premium / 100);
            Debug.Log(modelNr + ": " + json.values[0].module.make);
            Debug.Log("Form upload complete!");
        }
        yield return true;
    }
}
